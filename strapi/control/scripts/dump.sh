NOW=$(date "+%Y-%m-%d_%H:%M:%S")
pg_dump -U pm postgres > "${NOW}__v2-pm-db.sql"
echo "drop schema public cascade;
create schema public;
$(cat "${NOW}__v2-pm-db.sql")" > "${NOW}__v2-pm-db.sql"
mv "${NOW}__v2-pm-db.sql" /control/backups
