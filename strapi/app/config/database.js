module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'spi_db'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'postgres'),
        username: env('DATABASE_USERNAME', 'spi'),
        password: env('DATABASE_PASSWORD', 'fdb4e843'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
