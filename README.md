
![ide](<./create-application.png>)

---

#### PreRequis

- telecharger et installer python sur vos machines : https://www.python.org/downloads/
- installer le package  `requests` avec `pip install requests --user`

#### Task 1 - BackEnd - Tick & Store

**Avec un script python on vas remplir une table avec les valeurs retournees par un ticker.**

- creer un dossier `cryptos` a la racine du repo

- creer un fichier `ticker.py`

- dans le container strapi accessible via localhost:3337
    - creer une table `Cardano`
        - avec 1 champ `price` de type `float`

##### Script

#### v1-basic `ticker.py`
```python
# import librairie HTTP
import requests

# faire un call HTTP GET
r = requests.get('https://api.cryptowat.ch/markets/kraken/adausdt/price')
# demander le return json
rjs = r.json()
print(rjs)  # => {"result":{"price":1.244641}

# utiliser rjs pour completer data
r = requests.post('http://localhost:3337/cardanos', data={})
```


#### v2-getting_fancy `ticker.py`
```python
# import librairie HTTP


import requests


def exctractJsonFromResponse(response):
    resp_json = response.json()
    print(resp_json)
    return resp_json



def getTickerJson(symbol:str) -> dict:
    # faire un call HTTP GET
    r = requests.get(f'https://api.cryptowat.ch/markets/kraken/{symbol}/price')
    return exctractJsonFromResponse(r)



def addPriceToCardanoCollection(price:float) -> dict:
    # faire un call HTTP POST
    r = requests.post('http://localhost:3337/cardanos', data={'price' : price})
    return exctractJsonFromResponse(r)


t = getTickerJson('adausdt')
addPriceToCardanoCollection(t['result']['price'])


```

---

#### Task 2 - Front - Get & Display
**a la racine du repo**

##### creer une application quasar
- `quasar create webapp --branch v1`
    - laissez tout les champs par defaut a part
        - Axios
        - NPM (gestionnaire de paquet selectionner)
    - ![defaults](<./quasar-create.png>)

##### rentrez dans le dossier de l'application et lancer le daemon
- `cd webapp`
- lancer l'application `quasar dev`

##### ouvrez ce dossier dans vs-code
- ![ide](<./ide.png>)

##### ouvrir le fichier Index.vue
- **webapp/src/pages/Index.vue**
- faire les modifications necessaires a partir de ce template afin d'afficher les elements present dans la table cardano
```vue
<template>
  <q-page class="flex flex-center">
    <!-- declarons une liste -->
    <ul id="cardano-elements">
      <!-- iteration sur les elements present dans le tableau ticker -->
      <!-- creation d'un element li html a chaque iteration -->
      <!-- :key.id permets d'avoir un id unique pour chaque element html afin de les differencier -->
      <li v-for="price in ticker" :key="price.id">
        <ul id="cardano-element">
          <li> {{ price.id }} </li>
          <li> {{ price.price }} </li>
          <li> {{ price.created_at }} </li>
        </ul>
        
      </li>
    </ul>
  </q-page>

</template>

<script>
import { defineComponent } from 'vue';
import axios from 'axios'


export default defineComponent({
  name: 'PageIndex',
  // ajouter un champ data pour stocker de la donnee relative a cette page
  data () {
    return {
      ticker: null
    }
  },
  // effectuer un call http pour recuperer les elements present dans la table cardano
  mounted () {
    axios
      // modifier l'url pour recuperer tous les elements present dans la table cardano
      .get('http://localhost:3337/cardanos')
      .then(response => (this.ticker = response.data)) // on attribue le contenu du response.data dans ticker
  }
})
</script>

```

##### Ajouter un Chart
- npm install --save apexcharts
- npm install --save vue-apexcharts
```vue
<template>
  <q-page class="flex flex-center">
    <p> Test </p>
    <!-- declarons une liste -->
    <ul id="cardano-elements">
      <!-- iteration sur les elements present dans le tableau ticker -->
      <!-- creation d'un element li html a chaque iteration -->
      <!-- :key.id permets d'avoir un id unique pour chaque element html afin de les differencier -->
      <li v-for="price in ticker" :key="price.id">
        <ul id="cardano-element">
          <li> {{ price.id }} </li>
          <li> {{ price.price }} </li>
          <li> {{ price.created_at }} </li>
        </ul>
      </li>
    </ul>
    <div id="chart">
      <apexchart type="candlestick" height="350" :options="chartOptions" :series="series"></apexchart>
    </div>
  </q-page>
</template>

<script>
import axios from 'axios'
import VueApexCharts from 'vue-apexcharts'

export default {
  name: 'PageIndex',
  // ajouter un champ data pour stocker de la donnee relative a cette page
  components: {
    apexchart: VueApexCharts,
  },
  data () {
    return {
      ticker: null,
      series: [{
        data: [{
            x: new Date(1538778600000),
            y: [6629.81, 6650.5, 6623.04, 6633.33]
          },
          {
            x: new Date(1538780400000),
            y: [6632.01, 6643.59, 6620, 6630.11]
          },
          {
            x: new Date(1538782200000),
            y: [6630.71, 6648.95, 6623.34, 6635.65]
          },
          {
            x: new Date(1538784000000),
            y: [6635.65, 6651, 6629.67, 6638.24]
          },
          {
            x: new Date(1538785800000),
            y: [6638.24, 6640, 6620, 6624.47]
          },
          {
            x: new Date(1538787600000),
            y: [6624.53, 6636.03, 6621.68, 6624.31]
          },
          {
            x: new Date(1538789400000),
            y: [6624.61, 6632.2, 6617, 6626.02]
          },
          {
            x: new Date(1538791200000),
            y: [6627, 6627.62, 6584.22, 6603.02]
          },
          {
            x: new Date(1538793000000),
            y: [6605, 6608.03, 6598.95, 6604.01]
          },
          {
            x: new Date(1538794800000),
            y: [6604.5, 6614.4, 6602.26, 6608.02]
          },
        ]
      }],
      chartOptions: {
        chart: {
          type: 'candlestick',
          height: 350
        },
        title: {
          text: 'CandleStick Chart',
          align: 'left'
        },
        xaxis: {
          type: 'datetime'
        },
        yaxis: {
          tooltip: {
            enabled: true
          }
        }
      },
    }
  },
  // effectuer un call http pour recuperer les elements present dans la table cardano
  mounted () {
    axios
      // modifier l'url pour recuperer tous les elements present dans la table cardano
      .get('http://localhost:3337/cardanos')
      .then(response => (this.ticker = response.data)) // on attribue le contenu du response.data dans ticker
  }
}
</script>
```

---

#### Task 3 

**STRAPI**
##### Creer trois collections Graph , Candle, Volume avec les champs suivant :


###### Graph
- Relation : Has One Candle
- Relation : Has One Volume

###### Candle
- CloseTime: Int
- Open : float
- Low : float
- High : float
- Close : float

###### Voolume
- Volume : float
- QuoteVolume : float


**PYTHON**
##### Modifier `ticker.py` et ajouter cette fonction qui permets de recevoir la donnee pour alimenter notre chart
###### Query
```python
# import librairie HTTP
import requests

# ------------------------------------------------------------------------------
# TASK 1
# ------------------------------------------------------------------------------

def exctractJsonFromResponse(response):
    resp_json = response.json()
    # print(resp_json)
    return resp_json



# def getTickerJson(symbol:str) -> dict:
#     # faire un call HTTP GET
#     r = requests.get(f'https://api.cryptowat.ch/markets/kraken/{symbol}/price')
#     return exctractJsonFromResponse(r)



# def addPriceToCardanoCollection(price:float) -> dict:
#     # faire un call HTTP DELETE
#     r = requests.post('http://localhost:3337/cardanos', data={'price' : price})
#     return exctractJsonFromResponse(r)

# t = getTickerJson('adausdt')
# addPriceToCardanoCollection(t['result']['price'])

# ------------------------------------------------------------------------------
# TASK 3
# ------------------------------------------------------------------------------


def getOHLCCandleSticks(symbol:str):
    # faire un call HTTP GET
    r = requests.get(f'https://api.cryptowat.ch/markets/binance/{symbol}/ohlc')
    return exctractJsonFromResponse(r)

from pprint import pprint

data = getOHLCCandleSticks('adausdt')['result']



def saveCandle(close_time:int, open_price:float, low_price:float, hight_price:float, close_price:float) -> dict:
    r = requests.post('http://localhost:3337/candles', data=dict(
        CloseTime=close_time,
        Open=open_price,
        Low=low_price,
        High=hight_price,
        Close=close_price
    ))
    return exctractJsonFromResponse(r)



def saveVolume(volume:float, quote_volume:float) -> dict:
    r = requests.post('http://localhost:3337/volumes', data=dict(
        Volume=volume,
        QuoteVolume=quote_volume
    ))
    return exctractJsonFromResponse(r)



def saveGraph(candle_id:int, volume_id:int) -> dict:
    r = requests.post('http://localhost:3337/graphs', data=dict(
        candle=candle_id,
        volume=volume_id
    ))
    return exctractJsonFromResponse(r)

# ------------------------------------------------------------------------------
# PARTIE OBSCURE
# ------------------------------------------------------------------------------
print(type(data))
for k, v in data.items():
    if type(k) == dict:
        for k2, v2 in k.items():
            print(k2, v2)
    elif type(k) == str and k == "900":
        print(type(v))
        for l in v:
# ------------------------------------------------------------------------------
# FIN DE PARTIE OBSCURE
# ------------------------------------------------------------------------------
            print(l) # => [1474736400, 8744, 8756.1, 8710, 8753.5, 91.58314308, 799449.488966417]
            candle = l[:5]
            print(candle)
            candle = saveCandle(candle[0], candle[1], candle[2], candle[3], candle[4])
            volume = l[5:]
            print(volume)
            volume = saveVolume(volume[0], volume[1])
            graph = saveGraph(candle['id'], volume['id'])
            print(graph)


```
###### Template Response
```python
[1639489500, #CloseTime - GraphCandle
1.245, #OpenPrice - GraphCandle
1.245, #HighPrice - GraphCandle
1.227, #LowPrice - GraphCandle
1.229, #ClosePrice - GraphCandle
# -----
2448449.1, #Volume - GraphVolume
3026710.2704], #QuoteVolume - GraphVolume
```

##### 
**QUASAR**
- modifier le code dans Mounted() pour recuperer la donnee de la collection candle
```javascript
response.data.forEach(element => {

  console.log(element)
  let candle = element.candle
  let objData = {
    x: new Date(candle.CloseTime),
    y: [candle.Open, ...],
  }
  this.series[0].data.push(objData)

})
```

- https://apexcharts.com/vue-chart-demos/candlestick-charts/combo/#

---


##### Explore some more data visualisation library
- https://d3js.org/
- https://threejs.org/
